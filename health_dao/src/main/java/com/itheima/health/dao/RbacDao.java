package com.itheima.health.dao;

import com.itheima.health.pojo.Permission;
import com.itheima.health.pojo.Role;
import com.itheima.health.pojo.User;

import java.util.List;
import java.util.Map;

public interface RbacDao {
    //权限管理分页查询
    List<Role> findPage(String queryString);
    //预览设置
    Role findById(Integer id);
    //查询所有id
    List<Permission> findAll();
    //根据role_id查询role所对应的permission的id
    List<Integer> findPermissionsByRoleId(Integer id);

    //新增权限
    void add(Role role);

    void addRoleAndPermission(Map<String, Integer> map);

    void edit(Role role);
    /*先删除id在中间表（t_role_permission）中所对应的数据*/
    int deleteRoleAndPermissionRoleId(Integer id);

    /*在删除role表中的数据*/
    void deleteRole(Integer id);
}
