package com.itheima.health.service;

import com.itheima.health.entity.PageResult;
import com.itheima.health.pojo.Permission;
import com.itheima.health.pojo.Role;
import com.itheima.health.pojo.User;

import java.util.List;

public interface RbacService {
    PageResult findPage(Integer currentPage, Integer pageSize, String queryString);

    Role findById(Integer id);


    List<Permission> findAll();

    List<Integer> findPermissionsByRoleId(Integer id);
    //新增权限
    void add(Role role, Integer[] permissionsIds);

    void edit(Role role, Integer[] permissionsIds);

    void deleteRow(Integer id);
}
