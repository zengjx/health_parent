package com.itheima.health.job;

/**
 * @ClassName ClearPassOrderSetting
 * @Description 定时清理预约设置信息
 * @Author tzj
 * @Company 深圳黑马程序员
 * @Date 2019/10/27 22:19:23
 * @Version V1.0
 */

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.health.service.OrderSettingService;

public class ClearPassOrderSetting {

    @Reference
    OrderSettingService orderSettingService;
    public void clearOrderSetting(){
        orderSettingService.deletePassOrderSetting();
    }


}
