package com.itheima.health.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.itheima.health.constants.MessageConstant;
import com.itheima.health.dao.MemberDao;
import com.itheima.health.entity.Result;
import com.itheima.health.pojo.Member;
import com.itheima.health.service.MemberService;
import com.itheima.health.utils.MD5Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;

/**
 * @ClassName CheckItemServiceImpl
 * @Description TODO
 * @Author ly
 * @Company 深圳黑马程序员
 * @Date 2019/10/13 9:54
 * @Version V1.0
 */
@Service(interfaceClass = MemberService.class)
@Transactional
public class MemberServiceImpl implements MemberService {

    // 会员
    @Autowired
    MemberDao memberDao;


    @Override
    public Member findMemberByTelephone(String telephone) {
        return memberDao.findMemberByTelephone(telephone);
    }

    @Override
    public void add(Member member) {
        // 对密码进行md5加密保存
        if(member.getPassword()!=null){
            member.setPassword(MD5Utils.md5(member.getPassword()));
        }
        memberDao.add(member);
    }

    @Override
    public List<Integer> findMemberCountByRegTime(List<String> monthsList) {
        // 组织查询结果
        List<Integer> memberCountList = new ArrayList<>();
        for (String month : monthsList) {
            String regTime = month+"-31"; // 已知当前月，查询当前月的最后1天
            Integer memberCount = memberDao.findMemberCountByRegTime(regTime);
            memberCountList.add(memberCount);
        }
        return memberCountList;
    }

    /**
     * @FinctuonName getTemberSex
     * @Description TODO
     * @Author zengjx
     * @Date 2019/10/27 9:56
     * @Version V1.0
     * @return Map<String, Object>
     */
    @Override
  public   Map<String, Object> getTemberSex() {

        Map<String ,Object>  sexMap  = null;
        try {
            sexMap = new HashMap<>();
            List<String>  sexNameList=new ArrayList<>();
            sexMap.put("sex",sexNameList);
            //data
            Map<String ,Object>   map =memberDao.findSexCount();
            if(map==null){
                return null;
            }
            Iterator<String> iterator = map.keySet().iterator();
            while (iterator.hasNext()){
                sexNameList.add(iterator.next());
            }
           // Collections.addAll(sexNameList,map.get(0).);
            List<Map<String,Object>>  sexCountList =new ArrayList<>();

            BigDecimal  male=(BigDecimal)map.get(sexNameList.get(0));
            BigDecimal  fmale=(BigDecimal) map.get(sexNameList.get(1));
            Map<String ,Object>  maleMap=new HashMap<>();
            maleMap.put("name",sexNameList.get(0));
            maleMap.put("value",male);

            Map<String ,Object>  fmaleMap=new HashMap<>();
            fmaleMap.put("name",sexNameList.get(1));
            fmaleMap.put("value",fmale);

            sexCountList.add(maleMap);
            sexCountList.add(fmaleMap);
            sexMap.put("memberSexCount",sexCountList);

        } catch (Exception e) {
            e.printStackTrace();

        }
        return sexMap;
    }
/**
 * @FinctuonName getTemberSex
 * @Description TODO
 * @Author zengjx
 * @Date 2019/10/27 9:56
 * @Version V1.0
 * @return Map<String, Object>
 */
    @Override
    public Map<String, Object> getTemberAgeDivReport() {

        Map<String, Object> retMap = new HashMap<>();
        List<Map<String, Object>> getmap = memberDao.findAgeDivCount();
        try {
            //get   agedivname
            if (getmap == null || getmap.size() == 0) {
                return null;
            }
            List<String> listName = new ArrayList<>();
            for (int i = 0; i < getmap.size(); i++) {
                Map<String, Object> map = getmap.get(i);
                String name = (String) map.get("name");
                listName.add(name);
            }
            retMap.put("agedivname", listName);
            retMap.put("ageDivCount", getmap);
            return retMap;
        } catch (Exception e) {
            e.printStackTrace();

        }
        return null;
    }


}
