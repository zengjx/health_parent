package com.itheima.health.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.itheima.health.dao.RbacDao;
import com.itheima.health.entity.PageResult;
import com.itheima.health.pojo.CheckGroup;
import com.itheima.health.pojo.Permission;
import com.itheima.health.pojo.Role;
import com.itheima.health.service.RbacService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service(interfaceClass = RbacService.class)
public class RbacServiceImpl implements RbacService {

    @Autowired
    private RbacDao rbacDao;


    @Override
    public void add(Role role, Integer[] permissionsIds) {
        //1：组织基本信息数据，向检查组的表中添加1行数据
        rbacDao.add(role);
        //2：遍历checkitemIds的数组，获取检查项的id，同时和检查组的id，向检查项和检查组的中间表插入多行数据
        addRoleAndPermission(role.getId(), permissionsIds);
    }

    @Override
    public PageResult findPage(Integer currentPage, Integer pageSize, String queryString) {
        //1、初始化参数——将得到的当前页b,和页码要求的每页显示的数量为b,一起计算后内部传给PageInfo
        PageHelper.startPage(currentPage, pageSize);
        //2、查询数据
        List<Role> checkGroupList = rbacDao.findPage(queryString);
        /*for (Role user : checkGroupList) {
            System.out.println("====usergetPoster=====" + user.getDepartment() + "==" + user.getJobNumber() + user.getBirthday());
        }*/
        //3、封装PageHelp对应的结果集
        PageInfo<Role> pageInfo = new PageInfo<>(checkGroupList);
        return new PageResult(pageInfo.getTotal(), pageInfo.getList());
    }


    @Override
    public Role findById(Integer id) {
        Role role = rbacDao.findById(id);
        return role;
    }

    @Override
    public List<Permission> findAll() {
        List<Permission> permissionsList = rbacDao.findAll();
        return permissionsList;
    }

    @Override
    public List<Integer> findPermissionsByRoleId(Integer id) {
        return rbacDao.findPermissionsByRoleId(id);
    }

    @Override
    public void edit(Role role, Integer[] permissionsIds) {
        // 1：获取检查组的基本信息，执行更新
        rbacDao.edit(role);
        // 2：使用检查组的id，删除中间表的数据
        rbacDao.deleteRoleAndPermissionRoleId(role.getId());
        // 3：重新遍历checkitemIds的数组，获取检查项的id，同时和检查组的id，向检查项和检查组的中间表插入多行数据
        addRoleAndPermission(role.getId(), permissionsIds);
    }

    @Override
    public void deleteRow(Integer id) {
        //先删除id在中间表（t_role_permission）中所对应的数据
        int counts = rbacDao.deleteRoleAndPermissionRoleId(id);
        //在删除role表中的数据
        if (counts >= 0) {
            rbacDao.deleteRole(id);
        }
    }


    // 遍历checkitemIds的数组，获取检查项的id，同时和检查组的id，向检查项和检查组的中间表插入多行数据
    private void addRoleAndPermission(Integer roleId, Integer[] permissionsIds) {
        // 遍历
        for (Integer permissionsId : permissionsIds) {
            // 方案一：组织实体类，使用OGNL表达式解析
            // 方案二：Dao中传递多个参数，使用@Param
            //checkGroupDao.addCheckGroupCheckItem(checkGroupId,checkItemId);
            // 方案三：使用Map结构
            Map<String, Integer> map = new HashMap<>();
            map.put("role_Id", roleId);
            map.put("permissions_Id", permissionsId);
            rbacDao.addRoleAndPermission(map);
        }
    }
}
