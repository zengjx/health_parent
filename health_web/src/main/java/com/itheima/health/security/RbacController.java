package com.itheima.health.security;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.health.constants.MessageConstant;
import com.itheima.health.entity.PageResult;
import com.itheima.health.entity.QueryPageBean;
import com.itheima.health.entity.Result;
import com.itheima.health.pojo.CheckGroup;
import com.itheima.health.pojo.Permission;
import com.itheima.health.pojo.Role;
import com.itheima.health.pojo.User;
import com.itheima.health.service.RbacService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/rbac")
public class RbacController {

    @Reference
    private RbacService rbacService;


    // 新增
    @RequestMapping(value = "/add")
    public Result add(@RequestBody Role role, Integer[] permissionsIds){
        try {
            System.out.println("=======role=========" + role.getCreateTime());
            Date createTime = role.getCreateTime();
            rbacService.add(role,permissionsIds);
            return new Result(true, MessageConstant.ADD_CHECKGROUP_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.ADD_CHECKGROUP_FAIL);
        }
    }

    @RequestMapping("/findPage")
    public PageResult findPage(@RequestBody QueryPageBean queryPageBean) {
        PageResult pageResult = rbacService.findPage(queryPageBean.getCurrentPage(), queryPageBean.getPageSize(), queryPageBean.getQueryString());
        return pageResult;
    }

    @RequestMapping("/findById")
    public Result findById(Integer id) {
        Role role = rbacService.findById(id);
        if (role != null) {
            return new Result(true, MessageConstant.QUERY_CHECKGROUP_SUCCESS, role);
        } else {
            return new Result(false, MessageConstant.QUERY_CHECKGROUP_FAIL);
        }
    }

    @RequestMapping("/findAll")
    public Result findAll() {
        List<Permission> permissionList = rbacService.findAll();
        if (permissionList != null && permissionList.size() >0) {
            return new Result(true,MessageConstant.QUERY_CHECKGROUP_SUCCESS,permissionList);
        } else {
            return new Result(false,MessageConstant.QUERY_CHECKGROUP_FAIL);
        }
    }

    // 使用检查组id，查询检查组对应检查项的集合，对应json的数据格式是：[28,29,30,31,32]
    @RequestMapping(value = "/findPermissionsById")
    public List<Integer> findPermissionsByRoleId(Integer id){
        List<Integer> permissionIds = rbacService.findPermissionsByRoleId(id);
        return permissionIds;
    }

    @RequestMapping(value = "/edit")
    public Result edit(@RequestBody Role role,Integer[] permissionsIds){
        try {
            rbacService.edit(role,permissionsIds);
            return new Result(true, MessageConstant.EDIT_CHECKGROUP_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.EDIT_CHECKGROUP_FAIL);
        }
    }

    //删除某一条权限
    @RequestMapping("/deleteRow")
    public Result deleteRow(Integer id) {
        try {
            rbacService.deleteRow(id);
            return new Result(true,MessageConstant.DELETE_CHECKITEM_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,MessageConstant.DELETE_CHECKITEM_FAIL);
        }
    }

}
