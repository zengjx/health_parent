package com.itheima.health.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSONObject;
import com.itheima.health.constants.MessageConstant;
import com.itheima.health.entity.Result;
import com.itheima.health.pojo.Setmeal;
import com.itheima.health.service.SetmealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import redis.clients.jedis.JedisPool;

import java.util.List;

/**
 * @ClassName CheckItemController
 * @Description TODO
 * @Author ly
 * @Company 深圳黑马程序员
 * @Date 2019/10/13 9:56
 * @Version V1.0
 */
@RestController
@RequestMapping("/setmeal")
public class SetmealMobileController {

    @Reference
    SetmealService setmealService;
    @Autowired(required = false)
    JedisPool jedisPool;



    // 查询所有的套餐
    @RequestMapping(value = "/getSetmeal")
    public Result getSetmeal(){
        try {
            try {
                String setmealList = jedisPool.getResource().get("SetmealList");
                if (setmealList != null) {
                    List setmeals  = (List) JSONObject.parse(setmealList);
                    return new Result(true, MessageConstant.GET_SETMEAL_LIST_SUCCESS,setmeals);
                }
            } catch (Exception e) {
                e.printStackTrace();
                jedisPool.close();
            }
            List<Setmeal> list = setmealService.findAll();
            try {
                jedisPool.getResource().set("SetmealList",JSONObject.toJSONString(list));
            } catch (Exception e) {
                e.printStackTrace();
                jedisPool.close();
            }
            return new Result(true, MessageConstant.GET_SETMEAL_LIST_SUCCESS,list);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.GET_SETMEAL_LIST_FAIL);
        }
    }

    // 使用套餐id，查询套餐的详情
    @RequestMapping(value = "/findById")
    public Result findById(Integer id){
        try {
            try {
                String setmealInRedis = jedisPool.getResource().get("setmeal_redis"+id);
                if (setmealInRedis != null) {
                    Object parse = JSONObject.parse(setmealInRedis);
                    return new Result(true, MessageConstant.QUERY_SETMEAL_SUCCESS,parse);
                }
            } catch (Exception e) {
                e.printStackTrace();
                jedisPool.close();
            }
            Setmeal setmeal = setmealService.findById(id);
            try {
                jedisPool.getResource().set("setmeal_redis"+id,JSONObject.toJSONString(setmeal));
            } catch (Exception e) {
                e.printStackTrace();
                jedisPool.close();
            }
            return new Result(true, MessageConstant.QUERY_SETMEAL_SUCCESS,setmeal);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.QUERY_SETMEAL_FAIL);
        }
    }

}
